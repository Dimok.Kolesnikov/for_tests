const navbarList = [
    {name: 'Adaptive page', link: '/'},
    // {name: 'Home', link: '/home'},
    {name: 'Clock', link: '/clock'},
    {name: 'Drag Table', link: '/drag-table'}
];

export default navbarList;