import React from "react";

function Header() {
  return (
    <header>
      <nav>
        <input
          id="checkbox-menu"
          type="checkbox"
        />
        <label htmlFor="checkbox-menu">
          <ul className="menu touch">
            <li><a className="logo" href="#">luxestate</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Apartment</a></li>
            <li><a href="#">How it work</a></li>
            <li><a href="#">Agents</a></li>
            <li><a className="contacts" href="#">Contact Us</a></li>
            <li><a href="#">Join Us</a></li>
            <li><a href="#">Getting Started</a></li>
          </ul>
          <span className="toggle">☰</span>
        </label>
      </nav>
    </header>
  )
}

export default Header;
