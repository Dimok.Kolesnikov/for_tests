import Feact from "../Feact/Feact";

const AnotherComponent = Feact.createClass({
  componentWillMount(){
    console.log('AnotherComponent will mount')
  },

  componentDidMount(){
    console.log('AnotherComponent did mount')
  },

  render() {
    return Feact.createElement(
      'h1',
      null,
      this.props.message,
    )
  }
});


export default Feact.createElement(AnotherComponent, { message: 'Hello I am AnotherComponent' });

