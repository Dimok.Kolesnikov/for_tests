import Feact from "../../Feact/Feact";

function createFeactComponent(type, children) {
  // const classProps = Object.entries(children)
  return Feact.createClass({
    render() {
      return Feact.createElement(type, null, this.props.message)
    }
  })
}

export default function createComponent(type, props, children) {
  const feactClass = createFeactComponent(type, children)
  return Feact.createElement(feactClass, children)
}