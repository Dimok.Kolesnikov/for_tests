import Feact from "../Feact/Feact";
import AnotherComponent from "./AnotherComponent";

export default function TestComponent(message) {
  const Component = Feact.createClass({
    render() {
      return Feact.createElement('h1', null, this.props.message)
    }
  })

  return Feact.createElement(Component, { message, })
}

