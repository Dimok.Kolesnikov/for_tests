import React from 'react';
import { render } from "react-dom";
import './style/app.scss';

import Header from "./components/AdaptiveSite/Header";

function App(props) {
  return (
    <>
      <Header/>
    </>
  )
}

render(<App/>, document.getElementById('root'))