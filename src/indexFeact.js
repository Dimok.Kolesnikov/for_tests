import Feact from './Feact/Feact';
import TestComponent from "./components/TestComponent.js";
import AnotherComponent from "./components/AnotherComponent";

const feactElement = Feact.createElement('div', null, 'Simple Feact Element');


Feact.render(
  TestComponent('hello my Oleg'),
  document.getElementById('root'),
)

setTimeout(() => {
  Feact.render(
    TestComponent('hello my Oleg'),
    document.getElementById('root'),
  )
}, 3000);


