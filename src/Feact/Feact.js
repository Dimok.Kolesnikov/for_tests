import FeactCompositeComponentWrapper from "./FeactCompositeComponentWrapper";
import TopLevelWrapper from "./TopLevelWrapper";
import FeactReconciler from "./FeactReconciler";


function getTopLevelComponentInContainer(container) {
  return container.__feactComponentInstance;
}

function updateRootComponent(prevComponent, nextElement) {
  FeactReconciler.receiveComponent(prevComponent, nextElement)
}

function renderNewRootComponent(element, container) {
  const wrapperElement = Feact.createElement(TopLevelWrapper, element);
  const componentInstance = new FeactCompositeComponentWrapper(wrapperElement);

  const markUp = FeactReconciler.mountComponent(componentInstance, container);

  container.__feactComponentInstance = componentInstance._renderedComponent;
  return markUp;
}

const Feact = {

  createClass(spec) {

    function Constructor(props) {
      this.props = props;
    }

    Constructor.prototype = Object.assign(Constructor.prototype, spec);
    return Constructor;
  },

  createElement(type, props, children) {
    const element = {
      type,
      props: props || {},
    }
    element.props.children = children || null;
    return element;
  },

  render(element, container) {
    const prevComponent = getTopLevelComponentInContainer(container);
    if (prevComponent) {
      return updateRootComponent(prevComponent, element);
    } else {
      return renderNewRootComponent(element, container);
    }
  }
}

export default Feact;
