class FeactDomComponent {
  constructor(element) {
    this._currentElement = element;
  }

  mountComponent(container) {
    const { type, props } = this._currentElement;
    const { children } = props;
    const domElement = document.createElement(type);
    const textNode = document.createTextNode(children);
    domElement.appendChild(textNode);

    container.appendChild(domElement);
    this._hostNode = domElement;
    return domElement;
  }

  receiveComponent(nextElement) {
    const prevElement = this._currentElement;
    this.updateComponent(prevElement, nextElement);
  }

  updateComponent(prevElement, nextElement) {
    const lastProps = prevElement.props;
    const nextProps = nextElement.props;

    this._updateDomProperties(lastProps, nextProps);
    this._updateDomChildren(lastProps, nextProps);

    this._currentElement = nextElement;
  }

  _updateDomProperties(lastProps, nextProps) {
    // update css styles
  }

  _updateDomChildren(lastProps, nextProps) {
    const lastContent = lastProps.children;
    const nextContent = nextProps.children;

    if (!nextContent) {
      this.updateTextContent('')
    } else if (lastContent !== nextContent) {
      this.updateTextContent(` ${nextContent}`)
    }
  }

  updateTextContent(text) {
    const node = this._hostNode;
    const { firstChild, lastChild } = node;

    if (firstChild && firstChild === lastChild && firstChild.nodeType === 3) {
      firstChild.nodeValue = text;
      return;
    }
    node.textContent = text;
  }

}

export default FeactDomComponent;