import FeactDomComponent from "./FeactDomComponent";
import FeactReconciler from "./FeactReconciler";

class FeactCompositeComponentWrapper {
  constructor(element) {
    this._currentElement = element;
  }

  // MOUNT

  mountComponent(container) {
    const { type, props } = this._currentElement;

    const componentInstance = new type(props);
    this._instance = componentInstance;

    if (componentInstance.componentWillMount) {
      componentInstance.componentWillMount();
    }

    const markup = this.performInitialMount(container);

    if (componentInstance.componentDidMount) {
      componentInstance.componentDidMount();
    }

    return markup;
  }

  performInitialMount(container) {
    const renderedElement = this._instance.render();

    const child = instantiateFeactComponent(renderedElement);
    this._renderedComponent = child;

    return FeactReconciler.mountComponent(child, container);
  }

  // UPDATE

  receiveComponent(nextElement) {
    const prevElement = this._currentElement;
    this.updateComponent(prevElement, nextElement);
  }

  updateComponent(prevElement, nextElement) {
    const nextProps = nextElement.props;
    const inst = this._instance;

    if (inst.componentWillReceiveProps) {
      inst.componentWillReceiveProps(nextProps);
    }

    let shouldUpdate = true;

    if (inst.shouldComponentUpdate) {
      shouldUpdate = inst.shouldComponentUpdate(nextProps);
    }

    if (shouldUpdate) {
      this._performComponentUpdate(nextElement, nextProps);
    } else {
      inst.props = nextProps;
    }
  }

  _performComponentUpdate(nextElement, nextProps) {
    this._currentElement = nextElement;
    const inst = this._instance;

    inst.props = nextProps;

    this._updateRenderedComponent();
  }

  _updateRenderedComponent() {
    const prevComponentInstance = this._renderedComponent;
    const inst = this._instance;
    const nextRenderedElement = inst.render();
    FeactReconciler.receiveComponent(prevComponentInstance, nextRenderedElement);
  }
}


function instantiateFeactComponent(element) {
  const { type } = element;
  if (typeof type === 'string') {
    return new FeactDomComponent(element)
  } else if (typeof type === 'function') {
    return new FeactCompositeComponentWrapper(element)
  }
}

export default FeactCompositeComponentWrapper;