const FeactReconciler = {
  mountComponent(internalInstance, container) {
    return internalInstance.mountComponent(container);
  },

  receiveComponent(internalInstance, nextElement) {
    internalInstance.receiveComponent(nextElement);
  }
};


export default FeactReconciler;